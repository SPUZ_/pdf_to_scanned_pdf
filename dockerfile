FROM python:3

RUN apt-get clean && apt-get update \
    && apt-get install -y ghostscript \
    && apt-get clean && apt-get autoclean && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*


WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

VOLUME /usr/src/app/storage

ENV TELEGRAM_BOT_TOKEN=1
ENV USERS_ALLOWED=1

CMD [ "python", "./main.py" ]