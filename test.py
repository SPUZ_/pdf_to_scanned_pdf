import os
import random

filename = "2ea46f66a715e706e2f9ea4bfa93954f"

path_flat_filename = os.path.join(os.path.join(os.getcwd(), "storage"), "flat_" + filename + ".pdf")
path_scanned_filename = os.path.join(os.path.join(os.getcwd(), "storage"), "scan_" + filename + ".pdf")

os.system(f"convert "
                  f"-density 150 "
                  f"{path_flat_filename} "
                  f"-colorspace gray "
                  f"-linear-stretch 3.5%x10% "
                  f"-attenuate {random.uniform(0.2, 0.9)} "
                  f"+noise Gaussian "
                  f"-blur 0x{random.uniform(0.3, 0.6)} "
                  f"-rotate {random.uniform(0, 1)} "
                  f"{path_scanned_filename} ")