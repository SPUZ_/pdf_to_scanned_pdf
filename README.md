# PDF to Scanned PDF

## What is it

- Karen: You should sign this document
- SPUZ_: *Signs on iPad*
- Karen: Bruh, I mean print, sign, scan, send.
- SPUZ_: Ok-ok

## Screenshots

<img src="pic/working_bot.jpg" title="Working" width="600"/>

| Before                                                              | After                                                             |
|---------------------------------------------------------------------|-------------------------------------------------------------------|
| <img src="pic/before_1.jpg" title="Before first pic" width="600"/>  | <img src="pic/after_1.jpg" title="After first pic" width="600"/>  |
| <img src="pic/before_2.jpg" title="Before second pic" width="600"/> | <img src="pic/after_2.jpg" title="After second pic" width="600"/> |


## Build

- git clone
- docker build
- change TELEGRAM_BOT_TOKEN and USERS_ALLOWED (check .env.example)

## Inspired by

- https://github.com/rwv/lookscanned.io
- https://github.com/baicunko/scanyourpdf
