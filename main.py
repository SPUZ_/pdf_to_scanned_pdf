import os

from dotenv import load_dotenv
from telegramBot import TelegramBot

if __name__ == "__main__":
    load_dotenv()  # take environment variables from .env

    # Check if the .env variables are here
    telegram_bot_token = os.getenv("TELEGRAM_BOT_TOKEN")
    users_allowed = os.getenv("USERS_ALLOWED")

    if telegram_bot_token is None:
        print("add TELEGRAM_BOT_TOKEN to env")
        exit(1)
    if users_allowed is None:
        print("add USERS_ALLOWED to env")
        exit(1)

    users_allowed = users_allowed.split(",")
    try:
        users_allowed = [int(i) for i in users_allowed]
    except ValueError:
        print("I need only INT in USERS_ALLOWED")
        exit(1)

    telegram_bot = TelegramBot(token=telegram_bot_token, allowed=users_allowed)
    telegram_bot.run()
