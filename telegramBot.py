import asyncio
import hashlib
import os
import random

from telegram.constants import ChatAction
from telegram import Update, Message, File
from telegram.ext import (
    ApplicationBuilder,
    ContextTypes,
    CommandHandler,
    MessageHandler,
    filters,
)


async def runCmd(cmd: str):
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)
    _, _ = await proc.communicate()
    return proc


class TelegramBot:
    def __init__(self, token: dict, allowed: list[int]):
        self.bot_token = token
        self.allowed = allowed
        self.save_path = os.path.join(os.getcwd(), "storage")

    def hashing(self, file_name: str) -> str:
        return hashlib.md5(file_name.encode()).hexdigest()

    async def help(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
        Handles the /help
        """

        await context.bot.send_chat_action(
            chat_id=update.effective_chat.id, action=ChatAction.TYPING
        )

        await update.message.reply_text(
            "/start - Start the bot"
            "\nJust send .PDF file and I will ✨scan✨ it"
            "\nMade by @SPUZ_FEED"
        )

    def is_allowed(self, user: int) -> bool:
        """
        Check if user in allow list
        """
        return user in self.allowed

    async def start(self, update: Update, context: ContextTypes.DEFAULT_TYPE):
        """
        Handles the /start
        """
        if not self.is_allowed(update.message.from_user.id):
            return

        await context.bot.send_chat_action(
            chat_id=update.effective_chat.id, action=ChatAction.TYPING
        )

        await context.bot.send_message(
            chat_id=update.effective_chat.id, text="I'm a @SPUZ_FEED"
        )

    async def send_typing(
            self, update: Update, context: ContextTypes.DEFAULT_TYPE, every_seconds: int
    ):
        """
        Sends the typing action
        """

        if not self.is_allowed(update.message.from_user.id):
            return

        while True:
            await context.bot.send_chat_action(
                chat_id=update.effective_chat.id, action=ChatAction.TYPING
            )
            await asyncio.sleep(every_seconds)

    async def sending_document(
            self, update: Update, context: ContextTypes.DEFAULT_TYPE, every_seconds: int
    ):
        """
        Sends the uploading document action
        """

        if not self.is_allowed(update.message.from_user.id):
            return

        while True:
            await context.bot.send_chat_action(
                chat_id=update.effective_chat.id, action=ChatAction.UPLOAD_DOCUMENT
            )
            await asyncio.sleep(every_seconds)

    async def generate_document(
            self, update: Update, context: ContextTypes.DEFAULT_TYPE
    ):
        """
        Generates pdf file from input
        """
        if not self.is_allowed(update.message.from_user.id):
            return

        typing_task = context.application.create_task(
            self.send_typing(update, context, every_seconds=4)
        )

        file: File = await context.bot.get_file(update.message.document.file_id)
        typing_task.cancel()

        got_file_message: Message = await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Got file 📁"
        )

        filename = self.hashing(update.message.document.file_name)
        path_filename = os.path.join(self.save_path, filename + ".pdf")

        await got_file_message.edit_text(
            text="Got file 📁" "\nNow it has new filename: " + filename + ".pdf",
        )

        await file.download_to_drive(path_filename)

        saved_file_message: Message = await context.bot.send_message(
            chat_id=update.effective_chat.id,
            reply_to_message_id=got_file_message.id,
            text="Starting editing this file:"
                 "\nMaking pdf flat (iOS markup fix)"
                 "\nScanning document"
                 "\nResizing document"
                 "\n\nIt might take a while..."
        )

        path_flat_filename = os.path.join(self.save_path, "flat_" + filename + ".pdf")
        await runCmd(f"convert -density 300 {path_filename} {path_flat_filename}")

        await saved_file_message.edit_text(
            text="Starting editing this file:"
                 "\nMaking pdf flat (iOS markup fix) ✅"
                 "\nScanning document"
                 "\nResizing document"
                 "\n\nIt might take a while..."
        )

        path_scanned_filename = os.path.join(self.save_path, "scan_" + filename + ".pdf")
        attenuate = random.uniform(0.2, 0.4)
        blur = random.uniform(0.3, 0.6)
        rotate = random.uniform(0, 1)
        await runCmd(f"convert "
                     f"-density 150 "
                     f"{path_flat_filename} "
                     f"-colorspace gray "
                     f"-linear-stretch 3.5%x10% "
                     f"-blur 0x{blur} "
                     f"-rotate {rotate} "
                     f"-attenuate {attenuate} "
                     f"+noise Gaussian "
                     f"{path_scanned_filename} ")

        await saved_file_message.edit_text(
            text="Starting editing this file:"
                 "\nMaking pdf flat (iOS markup fix) ✅"
                 "\nScanning document ✅"
                 "\nResizing document"
                 "\n\nIt might take a while..."
        )

        path_ready_filename = os.path.join(self.save_path, "ready_" + filename + ".pdf")
        os.system(f"gs "
                  f"-dSAFER "
                  f"-dBATCH "
                  f"-dNOPAUSE "
                  f"-dNOCACHE "
                  f"-sDEVICE=pdfwrite "
                  f"-dPDFSETTINGS=/ebook "
                  f"-sColorConversionStrategy=LeaveColorUnchanged "
                  f"-dAutoFilterColorImages=true "
                  f"-dAutoFilterGrayImages=true "
                  f"-dDownsampleMonoImages=true "
                  f"-dDownsampleGrayImages=true "
                  f"-dDownsampleColorImages=true "
                  f"-sOutputFile={path_ready_filename} {path_scanned_filename}")

        await saved_file_message.edit_text(
            text="Starting editing this file:"
                 "\nMaking pdf flat (iOS markup fix) ✅"
                 "\nScanning document ✅"
                 "\nResizing document ✅"
                 "\n\nIt might take a while..."
        )

        sending_task = context.application.create_task(
            self.sending_document(update, context, every_seconds=4)
        )

        await context.bot.send_document(
            chat_id=update.effective_chat.id,
            reply_to_message_id=saved_file_message.id,
            caption=f"Attenuate: {attenuate}"
                    f"\nBlur: {blur}"
                    f"\nRotate: {rotate}"
                    f"\n\nDone ✨",
            document=open(path_ready_filename, 'rb')
        )

        sending_task.cancel()

    def run(self):
        """
        Run the bot
        """
        application = ApplicationBuilder().token(self.bot_token).build()

        application.add_handler(CommandHandler("start", self.start))
        application.add_handler(
            MessageHandler(
                filters.Document.MimeType("application/pdf"), self.generate_document
            )
        )

        application.run_polling()
